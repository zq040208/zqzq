package com.sky.service;

import com.sky.entity.User;
import com.sky.vo.OrderReportVO;
import com.sky.vo.SalesTop10ReportVO;
import com.sky.vo.TurnoverReportVO;
import com.sky.vo.UserReportVO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

public interface ReportService {
    /**
     * 获取营业额统计报表
     * @param begin
     * @param end
     * @return
     */
    TurnoverReportVO getTurnoverStatistics(LocalDate begin, LocalDate end);

    /**
     * 获取用户统计报表
     * @param begin
     * @param end
     * @return
     */
    UserReportVO getUserStatistics(LocalDate begin, LocalDate end);

    /**
     * 获取订单统计报表
     * @param begin
     * @param end
     * @return
     */
    OrderReportVO getorderStatistics(LocalDate begin, LocalDate end);

    /**
     * 获取销售排名前十的商品
     * @param begin
     * @param end
     * @return
     */
    SalesTop10ReportVO gettop10Statistics(LocalDate begin, LocalDate end);

    /**
     * 导出营业额统计报表
     * @param response
     */
    void exportBusinessData(HttpServletResponse response) throws IOException;
}
